//
//  LocationButtonApp.swift
//  LocationButton
//
//  Created by Mohamed Saeed on 12/04/2022.
//

import SwiftUI

@main
struct LocationButtonApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
