//
//  MapViewModel.swift
//  LocationButton
//
//  Created by Mohamed Saeed on 13/04/2022.
//

import MapKit

enum MapDetails{
    
    static let startingLocation = CLLocationCoordinate2D(latitude: 37.331516, longitude: -121.891054)
    
    static let defaultSpan = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
}

final class MapViewModel : NSObject, ObservableObject ,CLLocationManagerDelegate{
    
    @Published  var region=MKCoordinateRegion(center: MapDetails.startingLocation, span: MapDetails.defaultSpan )
    
    var locationManager: CLLocationManager?
    
    func checkIfLocationServicesIsEnabled(){
        if CLLocationManager.locationServicesEnabled(){
            locationManager = CLLocationManager()
          
            locationManager!.delegate = self
            locationManager!.desiredAccuracy = kCLLocationAccuracyBest
//            locationManager?.desiredAccuracy=kCLLocationAccuracyBest
            
        }else{
            print("Show an alert letting them knoe this is off and to go turn it on.")
        }
        
    }
    
    func checkLocationAuthorization(){
        guard let locationManager = locationManager else {
            return
        }
        
        switch locationManager.authorizationStatus{
            
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        case .restricted:
            print("your location is restricted likely due to parental controls.")
        case .denied:
            print("you have denied this aapp location permission. Go into setting to change it.")
        case .authorizedAlways, .authorizedWhenInUse:
            
            region = MKCoordinateRegion(center: locationManager.location!.coordinate , span:  MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05))
            
        @unknown default:
            break
        }

    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        checkLocationAuthorization()
    }
}

